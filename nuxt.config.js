export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'text_nuxt',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '網站前後端、網頁設計、程式語言教學網站' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ],
        script: [
            { src: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js' }
        ]

    },
    // env: {
    //     firebaseApiKey: 'AIzaSyDpNNLa0ZtdPy0YV1PCBEZQ7ozT3ww0-J0'
    // },
    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        '~/assets/scss/main.scss'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        { src: '~/plugins/dataPicker.js', mode: 'client' },
        { src: '~/plugins/gsap.js', mode: 'client' },
        { src: '~/plugins/gtag.js', mode: 'client' },
        { src: '~/plugins/axios.js' }
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        ['@nuxtjs/dotenv', { filename: '.env.' + process.env.NODE_ENV }] // 這是給其他 .env 使用
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        'cookie-universal-nuxt'
    ],
    // proxy: [
    //     'http://localhost:3034/api'
    // ],
    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {},
    loading: { color: 'red', height: '2px', duration: 5000 },
    transition: {
        name: 'layout',
        mode: 'out-in'
    },
    serverMiddleware: [
        { path: '/api', handler: '~/server/api.js' },
        { path: '/auth', handler: '~/server/auth.js' }
    ],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        // webpack
        loaders: {
            imgUrl: { limit: 1000 }
        },
        extractCSS: true,
        extend(config, ctx) {
            // const StylelintPlugin = require('stylelint-webpack-plugin')
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/,
                    options: {
                        fix: true
                    }
                })
                // config.plugins.push(
                //     new StylelintPlugin({
                //         files: ['**/*.vue', 'assets/**/*.scss'], // 要啟用自動修復的檔案路徑規則
                //         exclude: /(node_modules)/,
                //         fix: true
                //     })
                // )
            }
        }


        // extend(config, ctx) {
        // console.log(config.module.rules); //查看現在的 loader
        // config.module.rules.push({ //這段是範例，僅供參考
        //     test: /\.css$/,
        //     use: ['style-loader', 'postcss-loader']
        // })
        // }


    }

}
