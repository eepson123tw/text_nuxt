module.exports = {
    apps: [{
            name: 'text_nuxt', // 專案名稱
            script: './node_modules/nuxt-start/bin/nuxt-start.js', // nuxtstar套件的指向
            instances: 'max', // 負載平衡模式下的 cpu 數量 幾核心開幾個server
            exec_mode: 'cluster', // cpu 負載平衡模式
            max_memory_restart: '1G', // 緩存了多少記憶體重新整理
            port: 3001, // 指定伺服器上的 port
            env: {
                NODE_ENV: 'prod'
            }
        }]
        // , {
        //     name: 'text_nuxt_sit',
        //     script: './node_modules/nuxt-start/bin/nuxt-start.js',
        //     instances: 'max', // 負載平衡模式下的 cpu 數量
        //     exec_mode: 'cluster', // cpu 負載平衡模式
        //     max_memory_restart: '1G', // 緩存了多少記憶體重新整理
        //     port: 3002 // 指定伺服器上的 port
        // }],
        // deploy: {
        //     prod: {
        //         user: 'eepson123', // linux 登入帳號 帳號@ip
        //         host: ['35.189.179.241'], // 你的伺服器 ip
        //         ref: 'origin/master', // 分支
        //         repo: 'git@gitlab.com:eepson123tw/text_nuxt.git', // ssh 的 git
        //         path: '/home/eepson123/text_nuxt', // 伺服器上的路徑 //帳號名稱 //專案名稱
        //         'post-deploy': 'npm install && npm run build && pm2 reload ecosystem.config.js --env prod', // 佈署指令
        //         env: {
        //             NODE_ENV: 'prod'
        //         }
        //     }
        // }

}