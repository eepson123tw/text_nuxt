/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
import Cookie from 'js-cookie'
import jwtDecode from 'jwt-decode'
import API from '~/api.js'

// import * as types from '@/constants.js'
export const state = () => ({
    index: 0,
    data: {},
    userFavorite: null,
    coursesList: [],
    isUserLoggedIn: false, // 是否登入
    userPicture: '', // 會員照片
    userName: '', // 會員名稱
    userUid: '' // 會員 firebase 的 uid
})
export const actions = {
    nuxtServerInit({ commit, dispatch }, context) {
        // 這邊是給 Oauth 回來時提早觸發
        if (context.query.id_token && context.query.refresh_token) {
            const id_token_Decode = jwtDecode(context.query.id_token)
            commit('setUserLoggedIn', {
                id_token: context.query.id_token,
                refresh_token: context.query.refresh_token,
                userUid: id_token_Decode.user_id,
                userPicture: id_token_Decode.picture,
                userName: id_token_Decode.name
            })
            context.app.$cookies.set('id_token', context.query.id_token)
            context.app.$cookies.set('refresh_token', context.query.refresh_token)
            context.app.$cookies.set('userUid', id_token_Decode.user_id)
            context.app.$cookies.set('userPicture', id_token_Decode.picture)
            context.app.$cookies.set('userName', id_token_Decode.name)
        }
        dispatch('saveMemberInfo')
        if (context.app.$cookies.get('id_token')) {
            // nuxtServerInit 取得 cookie的方式和前端不同
            const picture = context.app.$cookies.get('userPicture')
            const name = context.app.$cookies.get('userName')
            const uid = context.app.$cookies.get('userUid')

            commit('setUserLoggedIn', {
                userPicture: picture,
                userName: name,
                userUid: uid
            })
        }
    },
    saveMemberInfo({ state }, payload) {
        const uid = (payload && payload.userUid) || state.userUid
        const _data = payload || {
            name: state.userName,
            picture: state.userPicture
        }
        return this.$axios({
            method: API.patchMemberInfo.method,
            url: API.patchMemberInfo.url.replace(':user_id.json', uid + '.json'),
            data: {
                ..._data
            }
        }).then((response) => {
            console.log(response.data, 'patchMemberInfo response')
        }).catch((error) => {
            console.log(error, 'error')
        })
    },

    setIdx({ commit }, data) { // 把commit 從context 解構出來
        commit('addNum', data)
    },
    async setCoursesList({ commit }) {
        const data = await this.$axios({
            method: API.getCoursesList.method,
            url: API.getCoursesList.url
        })
        let courses_array = []
        // 組成 state.courses 的格
        for (const key in data.data) {
            courses_array.push({
                id: key,
                ...data.data[key]
            })
        }
        courses_array = courses_array.sort((a, b) => {
            return a.order > b.order ? 1 : -1
        })
        commit('setList',
            courses_array
        )
    },
    getUserFavorite({ state, commit }, payload) {
        if (!state.isUserLoggedIn) { return }
        const uid = state.userUid
        return this.$axios({
            method: API.getMemberInfo.method,
            url: API.getMemberInfo.url.replace(':user_id.json', uid + '.json') + '?auth=' + this.$cookies.get('id_token')
        }).then((response) => {
            commit('set_userFavorite', response.data.favorite)
            console.log(state.userFavorite, 'state.userFavorite')
        }).catch((error) => {
            console.log(error, 'error')
        })
    },
    updateUserFavorite({ state }, payload) {
        return this.$axios({
            method: API.patchMemberInfo.method,
            url: API.patchMemberInfo.url.replace(':user_id.json', state.userUid + '.json') + '?auth=' + this.$cookies.get('id_token'),
            data: {
                favorite: state.userFavorite
            }
        }).then((response) => {
            console.log(response.data)
        }).catch((error) => {
            console.log(error)
        })
    }


}
export const mutations = {
    setUserLoggedIn(state, payload) {
        state.isUserLoggedIn = true
        state.userPicture = payload.userPicture || 'https://bulma.io/images/placeholders/128x128.png'
        state.userName = payload.userName
        state.userUid = payload.userUid
        if (process.client) {
            Cookie.set('id_token', payload.id_token)
            Cookie.set('refresh_token', payload.refresh_token)
            Cookie.set('userUid', state.userUid)
            Cookie.set('userPicture', state.userPicture)
            Cookie.set('userName', state.userName)
        }
    },
    setUserlogout: (state, payload) => {
        state.isUserLoggedIn = false
        state.userPicture = ''
        state.userName = ''
        Cookie.remove('id_token')
        Cookie.remove('refresh_token')
        Cookie.remove('userUid')
        Cookie.remove('userPicture')
        Cookie.remove('userName')
        $nuxt.$router.push({ name: 'index' })
    },

    addNum(state, payload) {
        state.index = payload
    },
    wowData(state, payload) {
        const { message, title } = payload
        state.data = { message, title }
    },
    setList(state, payload) {
        state.coursesList = payload
        // console.log(payload)
    },
    set_userFavorite: (state, payload) => {
        state.userFavorite = payload || {}
    }

}
export const getters = {
    getIdx(state) {
        return state.data
    },
    getCoursesList(state) {
        return state.coursesList
    }

}


export const namespaced = false
