const fs = require('fs')
const express = require('express')
const app = express()
const multer = require('multer')
const Mock = require('mockjs')
const sleep = require('../utils/sleep')

app.get('/test', (req, res) => {
    // res.status(401)
    const Random = Mock.Random
    const data = Mock.mock({
        'object|2': {
            310000: '上海市',
            320000: '江苏省',
            330000: '浙江省',
            340000: '安徽省'
        }
    })
    const img = Random.image('200x100', '#894FC4', '#FFF', 'png', '!')

    res.json({ title: img, message: data })
})


app.post('/pet/:id', (req, res) => {
    console.log(req.body, 'body')
    res.status(200)
    res.json({
        id: 0,
        category: {
            id: 0,
            name: 'string'
        },
        name: 'doggie',
        photoUrls: [
            'string'
        ],
        tags: [{
            id: 0,
            name: 'string'
        }],
        status: 'available'
    })
})


app.post('/form', async(req, res) => {
    await sleep(1000)
    console.log(req.body, 'body')
    res.status(200)
    res.json({
        id: 0,
        category: {
            id: 0,
            name: 'string'
        },
        name: 'doggie',
        photoUrls: [
            'string'
        ],
        tags: [{
            id: 0,
            name: 'string'
        }],
        status: 'available'
    })
})


app.get('/courses', (req, res) => {
    res.json({
        courses: [{
            id: 1,
            name: '進入 python 的魔法世界 - 第一次學程式入門課',
            color: '#b5b5ac',
            img: 'https://bulma.io/images/placeholders/1280x960.png',
            description: '這堂課使用python turtle 模組來創造幾何畫作，課程使用繪圖指令來講述程式語言的重要觀念，讓你打好程式語言的基礎。',
            introduction: ''
        }, {
            id: 2,
            name: '基礎教學資訊科技基礎教學',
            color: '#b5b5ac',
            img: 'https://bulma.io/images/placeholders/1280x960.png',
            description: '這堂課教導基礎教學資訊科技基礎教學。',
            introduction: ''
        }, {
            id: 3,
            name: 'illustrator基礎教學',
            color: '#f52e36',
            img: 'https://bulma.io/images/placeholders/1280x960.png',
            description: '這堂課教導illustrator。',
            introduction: ''
        }]

    })
})

app.post('/file', (req, res, next) => {
    const dir = 'uploads'
    if (!fs.existsSync(dir)) { fs.mkdirSync(dir) }
    const storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, './' + dir + '/')
        },
        filename: (req, file, cb) => {
            const name = file.originalname
            const ext = name.split('.')[name.split('.').length - 1]
            cb(null, Date.now() + '.' + ext)
        }
    })
    const upload = multer({ storage }).single('videoFile')

    // eslint-disable-next-line handle-callback-err
    upload(req, res, (err) => {
        // 回傳值
        res.status(200)
        res.json({
            code: 200,
            message: '上傳成功'
        })
    })
})


module.exports = app
