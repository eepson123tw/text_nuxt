// const http = require('http')
const express = require('express') // 引入express框架
const test = require('./router/test') // 引入路由 test
const app = express() // 將框架實體導出
const port = process.env.PORT || 3034

const nuxt = require('./router/nuxt.js')


app.use(express.json()) // 接收 json 資料
app.use(express.urlencoded({ extended: false })) // 接收 form urlencoded 的資料

app.listen(port, () => console.log(`mock server listening at http://localhost:${port}`)) // 讓node聽port

// 解決cors問題
const cors = require('./utils/cors.js')
app.use('/*', cors) // 所有路由引入cors
    // 所有路由只要被重製就會印出時間
app.use((req, res, next) => {
    console.log('Time:', Date.now())
    next()
})
app.use('/', nuxt)

app.get('/', (req, res) => res.send('Hello World!'))

app.use('/api', test) // 將路由定義 /api+{/test來的所有路徑 已於上方引入}


// http.createServer(async(req, res) => {
//     await sleep(3000)
//     console.log(req, 'req')
//     res.end('<p>Hello w12313or1233333312312313123ld</p>')
// }).listen(process.env.PORT || 3000)

// console.log(process.env,"process.env")