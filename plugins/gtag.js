import Vue from 'vue'
import VueGtag from 'vue-gtag'


Vue.use(VueGtag, {
    config: { id: 'G-DLQM3EZGL4' },
    appName: 'APP_NAME'
})

// if (process.env.NODE_ENV === 'production') {
//     Vue.use(VueGtag, {
//         config: { id: 'G-DLQM3EZGL4' }

//     })
// }
